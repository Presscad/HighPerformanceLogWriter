﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighPerformanceLog
{
    public class Log
    {
        /// <summary>
        /// The occuring time.
        /// </summary>
        public DateTime Time { get; set; } 

        /// <summary>
        /// Log的类型
        /// </summary>
        public int LogType { get; set; }

        /// <summary>
        /// Log的参数列表
        /// </summary>
        public string[] Arguments { get; set; }


        // Used to separate data fields of a log when serializing.
        char separator = (char)6;

        /// <summary>
        /// Construct a Log object.
        /// </summary>
        /// <param name="logType">The type of log.</param>
        /// <param name="args">Argument list.</param>
        public Log(int logType, params object[] args)
        {
            Time = DateTime.Now;
            LogType = logType;
            Arguments = new string[args == null ? 0 : args.Length];
            for (int i = 0; i < Arguments.Length; i++)
                Arguments[i] = args[i] == null ? "" : args[i].ToString();
        }

        /// <summary>
        /// 格式: TimeDiff|LogType|Arguments。其中 TimeDiff 为 Time 与 2019/01/01 00:00:00 的毫秒差。
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append((long)(Time - new DateTime(2019, 1, 1)).TotalMilliseconds);
            sb.Append(separator);
            sb.Append(LogType);
            if (Arguments != null)
                for (int i = 0; i < Arguments.Length; i++)
                {
                    sb.Append(separator);
                    sb.Append(Arguments[i]);
                }
            return sb.ToString();
        }

        static Random random = new Random();
        static string[] types = { "LOG", "DBG", "SYS", "CRT", "ERR", "MSG" };

        /// <summary>
        /// 随机生成一个Log。
        /// </summary>
        /// <returns></returns>
        public static Log Random()
        {
            return new Log(random.Next(10, 99), types[random.Next(types.Length)],
                "The content of the log.1234567890123456789012345678901234567890123456789012345678");
        }
    }
}