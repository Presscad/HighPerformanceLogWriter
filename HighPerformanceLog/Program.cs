﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HighPerformanceLog
{
    class Program
    {


        static void Main(string[] args)
        {
            LogWritingSpeedTest();
            // AddTest();
        }

        static void AddTest()
        {
            Test tb1 = new Test()
            {
                TestName = "初始化日志数组",
                TestMethod = (len) =>
                {
                    int blockSize = (int)len;
                    Log[] logArray = new Log[blockSize];
                    for (int i = 0; i < logArray.Length; i++)
                        logArray[i] = Log.Random();
                    DateTime t2 = DateTime.Now;
                    return logArray;
                }
            };

            Test tb2 = new Test()
            {
                TestName = "",
                TestMethod = (arg) =>
                {
                    HPLogWriter hw = new HPLogWriter();
                    DateTime dt1 = DateTime.Now;
                    hw.Start();
                    for (int i = 0; i < 1000; i++)
                    {
                        foreach (Log log in (arg as Log[]))
                            hw.Add1(log);
                    }
                    hw.Stop();
                    DateTime dt2 = DateTime.Now;

                    return (dt2 - dt1).TotalMilliseconds;
                }
            };

            int size = 100_000;
            ExecutionResult er1 = tb1.Apply(size, 1);

            for (int i = 0; i < 10; i++)
            {
                double time = (double)tb2.TestMethod(er1.ReturnedValue);
                double speed = size / time;
                Console.WriteLine("Average Execution Time: {0} ms.", time.ToString("0.000"));
                Console.WriteLine("Adding Speed: {0} million records per second.", speed.ToString("0.000"));
            }


        }


        static void LogWritingSpeedTest()
        {
            FileInfo fi = new FileInfo(Path.GetFileName("system.log"));
            int blockSize = 1024;
            int runningTimes = 10 * 1024;

            DateTime t1 = DateTime.Now;
            Log[] logs = new Log[blockSize];
            for (int i = 0; i < logs.Length; i++)
                logs[i] = Log.Random();
            DateTime t2 = DateTime.Now;

            Console.WriteLine("Log File: {0}", fi.FullName);
            Console.WriteLine("# of Logs: {0}", blockSize * runningTimes);
            Console.WriteLine("Content of logs[0]: {0}", logs[0].ToString());
            Console.WriteLine("Length of logs[0]: {0}", logs[0].ToString().Length);

            DateTime t3 = DateTime.Now;
            HPLogWriter lm = new HPLogWriter(fi.FullName);
            lm.Start();
            for (int times = 0; times < runningTimes; times++)
                for (int i = 0; i < logs.Length; i++)
                    lm.Add(logs[i]);
            lm.Stop();
            DateTime t4 = DateTime.Now;

            Console.WriteLine("Init: {0}", (t2 - t1).TotalSeconds.ToString("0.000s"));
            Console.WriteLine("Save: {0}", (t4 - t3).TotalSeconds.ToString("0.000s"));
            Console.WriteLine("Speed: {0} M/s", (blockSize * runningTimes / 1000 / (t4 - t3).TotalMilliseconds).ToString("0.000"));
            Console.WriteLine($"Log Size: {fi.Length / 1024f / 1024} MB");
            File.Delete(fi.FullName);
        }
    }
}
