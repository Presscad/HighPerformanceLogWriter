﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HighPerformanceLog
{
    public class LogToStringProcessor
    {
        public object[] Objects { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
        public string Result { get; set; }
         

        public LogToStringProcessor(object[] logs, int start, int end)
        {
            this.Objects = logs;
            this.Start = start;
            this.End = end;
        }

        public void Process()
        {
            StringBuilder sb = new StringBuilder();
            for (int j = Start; j < End; j++)
            {
                if (Objects[j] == null)
                    break;
                sb.Append(Objects[j].ToString());
                sb.Append('\n');
            }
            Result = sb.ToString();
        }
    }
}
