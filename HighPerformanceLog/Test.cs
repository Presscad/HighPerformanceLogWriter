﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighPerformanceLog
{

    public delegate object TestDelegator(object arg);

    public class Test
    {
        public string TestName { get; set; }
        public TestDelegator TestMethod { get; set; }

        public ExecutionResult Apply(object input, int runningTimes = 10)
        {
            if (TestMethod == null)
                return new ExecutionResult();
            object r = null;
            // 先运行三次热身
            for (int i = 0; i < 3; i++)
                r = TestMethod(input);


            DateTime t1 = DateTime.Now;
            for (int rt = 0; rt < runningTimes; rt++)
                r = TestMethod(input);


            DateTime t2 = DateTime.Now;

            return new ExecutionResult() { RunningTime = (t2 - t1).TotalMilliseconds / runningTimes, ReturnedValue = r };
        }
    }
}
