﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighPerformanceLog
{
    public class ExecutionResult
    {
        public object ReturnedValue { get; set; }
        public double RunningTime { get; set; } = 0;

        public override string ToString()
        {
            return $"Running Time: {RunningTime} ms.";
        }
    }
}
